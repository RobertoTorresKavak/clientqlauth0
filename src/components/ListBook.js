import React from 'react';
import { Query } from "react-apollo";
import { gql } from "apollo-boost";
import '../App.css';

const LIST_BOOKS = gql`
  query {   
    makes(skip : 0, limit : 0) {
      id,
      code,
      name
    }
  }
`

export default () => (
  <Query query={LIST_BOOKS}>
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>{"Error..." + error}</p>;

      return (
        <div className="col-sm-12">
          {!loading &&
            data.makes.map(make => (
              <div className="col-sm-4" key={make.id}>
                <div className='pa3 bg-black-05 ma3'>
                  
                  <div>
                    <div className='movie'>
                      <h3 align="center"> { make.name }&nbsp; </h3>
                      <h4 align="center">DataBase code:  { make.code } </h4>
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>
      );
    }}
  </Query>
);